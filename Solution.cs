class Solution
{
    public static int[] solution(string[] A, string[] B)
    {
        var forbidden = new HashSet<string>();
        foreach (string host in B)
        {
            forbidden.Add(host);
        }
        var allowed = new HashSet<string>();
        for (int i = 0; i < A.Length; i++)
        {
            if (IsAllowed(A[i], forbidden, allowed))
            {
                allowed.Add(A[i]);
            }
        }
        var resultArray = new int[allowed.Count];
        var index = 0;
        foreach (string host in allowed)
        {
            resultArray[index] = Array.IndexOf(A, host);
            index++;
        }
        return resultArray;
    }

    private static bool IsAllowed(string host, HashSet<string> forbidden, HashSet<string> allowed)
    {
        if (forbidden.Contains(host))
        {
            return false;
        }
        var dotIndex = host.IndexOf('.');
        while (dotIndex != -1)
        {
            var parent = host.Substring(dotIndex + 1);
            if (forbidden.Contains(parent) || allowed.Contains(parent))
            {
                return false;
            }
            dotIndex = host.IndexOf('.', dotIndex + 1);
        }
        return true;
    }
}